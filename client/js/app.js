'use strict';
(function() {
	let app = angular.module('ADonis', [
	'timeline-controller', 
	'account-memberships-controller', 
	'syncreport-controller', 
	'ngRoute']);
	
	app.config(function ($routeProvider,$locationProvider) {
			$locationProvider.hashPrefix(''); 
      $routeProvider
      .when('/', {
        template: 'views/home.html'
      })
      .when('/account/timeline/:VUnetID', {
        templateUrl: 'views/accountTimeline.html',
				controller: 'timelineController'
      })
			.when('/account/memberships/:VUnetID', {
        templateUrl: 'views/accountMemberships.html',
				controller: 'accountMembershipsController'
      })
			.when('/syncreport', {
        templateUrl: 'views/syncReport.html',
				controller: 'syncReportController'
      });
    });
})();