'use strict';

var today = new Date();

(function() {
  let app = angular.module('syncreport-controller', ['lbServices']);

	app
	.directive('compile', ['$compile', function ($compile) {
		return function(scope, element, attrs) {
			scope.$watch(
				function(scope) {
					return scope.$eval(attrs.compile);
				},
				function(value) {
					element.html(value);
					$compile(element.contents())(scope);
				}
		 )};
		}])
	.controller('syncReportController', function($scope, $routeParams, Adsynclog) {
		Adsynclog.getLast(null,(data, err) => {
			
			$scope.date = data.date;
			
			$scope.userAddCount = data.userAddCount;
			$scope.userAddList = JSON.parse(data.userAddList).sort();
			$scope.userUpdateCount = data.userUpdateCount;
			$scope.userUpdateList = JSON.parse(data.userUpdateList).sort();
			$scope.userRemoveCount = data.userRemoveCount;
			$scope.userRemoveList = JSON.parse(data.userRemoveList).sort();

			$scope.groupAddCount = data.groupAddCount;
			$scope.groupAddList = JSON.parse(data.groupAddList).sort();
			$scope.groupUpdateCount = data.groupUpdateCount;
			$scope.groupUpdateList = JSON.parse(data.groupUpdateList).sort();
			$scope.groupRemoveCount = data.groupRemoveCount;
			$scope.groupRemoveList = JSON.parse(data.groupRemoveList).sort();
			
			$scope.uigAddCount = data.uigAddCount;
			$scope.uigAddList = JSON.parse(data.uigAddList).sort();
			$scope.uigRemoveCount = data.uigRemoveCount;
			$scope.uigRemoveList = JSON.parse(data.uigRemoveList).sort();	
			
			$scope.gigAddCount = data.gigAddCount;
			$scope.gigAddList = JSON.parse(data.gigAddList).sort();		
			$scope.gigRemoveCount = data.gigRemoveCount;
			$scope.gigRemoveList = JSON.parse(data.gigRemoveList).sort();
			
			$scope.outputlog = ""
			var el = document.getElementById('console');
			

			
			$scope.parseUserList = function(list) {
				var result = "<ul>";
				
				for(var i=0;i<list.length;i++){
					result += '<li><a href="/#/account/timeline/'+list[i]+'">'+list[i]+'</a></li>'
				}
				result += "</ul>";
				$scope.outputlog = result;
			}
			
			$scope.parseGroupList = function(list) {
				var result = "<ul>";
				
				for(var i=0;i<list.length;i++){
					result += '<li>'+list[i]+'</li>'
				}
				result += "</ul>";
				$scope.outputlog = result;
			}
			
			$scope.parseUIGList = function(list) {
				var result = "<ul>";
				
				for(var i=0;i<list.length;i++){
					result += '<li><a href="/#/account/timeline/'+list[i][0]+'">'+list[i][0]+'</a>: '+list[i][1]+'</li>';
				}
				result += "</ul>";
				$scope.outputlog = result;
			}
			
			$scope.parseGIGList = function(list) {
				var result = "<ul>";
				
				for(var i=0;i<list.length;i++){
					result += '<li>'+list[i][0]+' : '+list[i][1]+'</li>';
				}
				result += "</ul>";
				$scope.outputlog = result;
			}
		
			let syncReportController = this;
		});
	});
})();