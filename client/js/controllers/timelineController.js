'use strict';

var today = new Date();
var minDate = today;

(function() {
  let app = angular.module('timeline-controller', ['lbServices']);

	app.controller('timelineController', function($scope, $routeParams, Adaccount, Adgroup) {
		Adaccount.find({
			filter: {
					where: {
						cn: $routeParams.VUnetID
					}
			}
		}, (data, err) => {
			Adgroup.groupsByUIG({'id': data[0].objectGUID}, (data, err) => {
				var parsed = parseGroupsObject(data);
				var timelineData = parseObjectToTimeline(parsed);
				
				var tlc = TimelinesChart()
					.data(timelineData)
					.zQualitative(true)
					.zColorScale(getColorScale())
					.leftMargin(400)
					.topMargin(0)
					.rightMargin(400)
					.useUtc(true)
					.enableAnimations(false)
					.maxHeight(2000)
					.zoomX([minDate, Date.now()])
					(document.getElementById('myPlot'));
			});
		});
		
		$scope.account = $routeParams.VUnetID;

		let adAccountController = this;
	});
})();

function parseGroupsObject(data) {
	var result = {};
	for(var i=0;i<data.length;i++) {
		if (data[i].category in result) {
			if (data[i].cn in result[data[i].category]) {
				result[data[i].category][data[i].cn].push(data[i]);
			} else {
				result[data[i].category][data[i].cn] = [data[i]];
			}
		} else {
			result[data[i].category] = {[data[i].cn]: [data[i]]};
			
		}
	}
	return result;
}

function parseObjectToTimeline(data) {
	var result = [];
	var dataKeys = Object.keys(data);
	var dateMin = null;
	
	for(var i=0;i<dataKeys.length;i++) {
		var gr = {};
		gr.group = dataKeys[i];
		
		var grKeys = Object.keys(data[dataKeys[i]]);
		var grData = [];
		for (var j=0; j<grKeys.length; j++) {
			var obj = {};
			obj.label = grKeys[j];
			var objData = [];
			for (var k=0; k<data[dataKeys[i]][grKeys[j]].length; k++) {
				objData.push(createTimeRecord(data[dataKeys[i]][grKeys[j]][k],dataKeys[i]));
			}
			obj.data = objData;
			grData.push(obj);
		}
		gr.data = grData;
		result[i] = gr;
	}

	return result;
}

function getColorScale() {
	var objArr=[
			{label:"Employee"},
			{label:"Student"},
			{label:"Fileserver"},
			{label:"Mail"},
			{label:"Printing"},
			{label:"Software"},
			{label:"Resources"},
			{label:"Cloud"},
			{label:"Citrix"},
			{label:"Website"},
			{label:"Service"},
			{label:"Server"},
			{label:"Computer"},
			{label:"System"},
			{label:"Unspecified"}
		];//Need to attach different colors to each object in arry
	const map1 = objArr.map(x => x.label);
	var colScale= d3.scale.ordinal().range([
		"#1F78B4",
		"#FF7D0A",
		"#2AA12D",
		"#D52728",
		"#9467BC",
		"#8D564A",
		"#E577C0",
		"#7F7F7F",
		"#BCBD24",
		"#18BDD0",
		"#AEC6E8",
		"#FFBC78",
		"#97DF89",
		"#FF9693",
		"#C2B0D7",
		"#C49C93",
		"#F7B7D2",
		"#C6C8C5",
		"#DBDA90",
		"#9EDAE5"]).domain(map1);
					
	return colScale;
}

/*function timelineParse(data) {
	var memberOf = {};
	memberOf.group = "";
	if(data) {
		var counter = 0;
		var currentCN = "";
		var result = [];

		while(data.length != counter) {	
			var group = {};
			if(data[counter].cn != currentCN) {
				currentCN = data[counter].cn;
				group.label = currentCN;
			}
				
			var timestamps = [];
			if((counter+1) >= data.length) {
				timestamps.push(createTimeRecord(data[counter]));
				counter++;
			} else {
				if(data[counter+1].cn != currentCN) {
					timestamps.push(createTimeRecord(data[counter]));
					counter++;
				} else {
					while(data[counter+1].cn == currentCN) {
						timestamps.push(createTimeRecord(data[counter]));
						counter++;
					}
				}
			}
			group.data = timestamps;
			result.push(group);
		}
	}		
	memberOf.data = result;
	return [memberOf];
}*/

function createTimeRecord(data, category) {
	var record = {};
	// Split method is used to strip the time-part out of the date.
	var start = new Date(data.row_start.split('T')[0]);
	var end = new Date(data.row_end.split('T')[0]);
	
	if(start < minDate) {
		minDate = start;
	}
	
	if(end > today) {
		end = today;
	}
	record.timeRange = [start, end];
	//record.val = data.cn;
	record.val = category;
	return record;
}