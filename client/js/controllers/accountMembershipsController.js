'use strict';

(function() {
  let app = angular.module('account-memberships-controller', ['lbServices']);	

	app.controller('accountMembershipsController', function($scope, $routeParams, Adaccount) {
		Adaccount.groupsByUIG({'account': $routeParams.VUnetID}, (data, err) => {
			$scope.root = unflatten(data,$routeParams.VUnetID);
			var root = unflatten(data,$routeParams.VUnetID);
			var dimensions = analyzeMemberships(data);
			var maxNameLengths = calculateMaxNameLengths(data,$routeParams.VUnetID);
			var leafCount = dimensions;
			
			var margin = {
                top: 40,
                right: 50,
                bottom: 20,
                left: 100
			}
			
			var i = 0,
					duration = 750;
			
			var height = leafCount * 12 + margin.top + margin.bottom;
			//var width =  $(window).width() - margin.left - margin.right;
			var width =  maxNameLengths[maxNameLengths.length-1] + margin.left + margin.right;
			
			d3.select("#membershipTree").select("svg").remove();
			
			var tree = d3.layout.tree()
				.separation(function(a, b) { return ((a.parent == tree) && (b.parent == tree)) ? 3 : 1; });
				//.size([height, width]);
			
			tree.nodeSize([12,350]);
				
			var diagonal = d3.svg.diagonal()
				.projection(function(d){ 
					return [d.y, d.x]; 
				}); // flip x and y of links
			
			var svg = d3.select("#membershipTree").append("svg")
					.attr("width",  width)
					.attr("height", height + margin.top + margin.bottom)
				.append("g")
					.attr("transform", "translate("+margin.left+","+(leafCount * 12 / 2)+")");
					
			root.x0 = height / 2;
			root.y0 = 0;
			
			function createButtons() {
				var placeholder = d3.select("svg").append("g");

				placeholder.append("rect")	
					.attr("x", 10)
					.attr("y", 10)
					.attr("width", 170)
					.attr("height", 20)
					.attr("id","inheritedButton")
					.style("fill", "#4ABDAC")
				placeholder.append("text")
					.attr("x", 95)
					.attr("y", 10)
					.attr("dy", "1em")
					.attr("id","inheritedButtonText")
					.style("fill", "white")	
					.style("text-anchor", "middle")					
					.text("Show inherited groups")
				.on("click", toggleInherited)
				.on("mouseover", inheritedMO)
				.on("mouseout", inheritedO);
					

			}
			
			createButtons();
		
			function update(source) {
				// Compute the new tree layout.
				var nodes = tree.nodes(root);
				var links = tree.links(nodes);
				
				// Normalize for fixed-depth.
				nodes.forEach(function(d) {
					d.y = maxNameLengths[d.depth]; 
				});
				
				// Update the nodes…
				var node = svg.selectAll("g.node")
					.data(nodes, function(d) { return d.id || (d.id = ++i); });
					
				// Enter any new nodes at the parent's previous position.
				var nodeEnter = node.enter().append("g")
						.attr("class", "node")
						.attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
						.on("click", click)
						.on("mouseover", mouseover)
						.on("mouseout", mouseout);
						
				nodeEnter.append("circle")
					.attr("r", 1e-6)
					.attr("fill", function(d) { return colorMap[d.category]; })
					.style("stroke", function(d) { return colorMap[d.category]; })
				
				nodeEnter.append("text")
					.attr("x", function(d) { return d.children ? -8 : 8; })
					.attr("dy", ".35em")
					.attr("class", "shadow")	
					.style("text-anchor", function(d){ return d.children ? "end" : "start"; })
					.style("fill-opacity", 1e-6)					
					.text(function(d) { return d.name; })
					.call(getBB);   
				nodeEnter.insert("rect","text")
						.attr("width", function(d){return d.bbox.width})
						.attr("height", function(d){return d.bbox.height})
						.style("fill", "white")
						.style("fill-opacity", 1e-6);
			
				// Transition nodes to their new position.
				var nodeUpdate = node
					.transition()
					.duration(duration)
					.attr("transform", function(d) { 
						return "translate(" + d.y + "," + d.x + ")"; 
					});

				nodeUpdate
					.select("circle")
					.attr("r", function(d){ return d._children ? 4 : 5})
					.attr("fill", function(d){ return d.children ? colorMap[d.category] : "#fff"})
					.style("stroke-width", function(d){ return d._children ? 3.5 : 1});

				node.select("text")
					.transition()
					.duration(duration)
					.attr("x", function(d) { return (d.children || d._children) && !d.parent ? -8 : 8; })
					.style("text-anchor", function(d){ return (d.children || d._children) && !d.parent ? "end" : "start"; })
					.style("fill-opacity", 1);				
				node.select("rect")
				.transition()
				.duration(duration)
				.attr("x", function(d) { return (d.children || d._children) && !d.parent ? -8 : 8; })
				.style("text-anchor", function(d){ return (d.children || d._children) && !d.parent ? "end" : "start"; })
				.style("fill-opacity", function(d){ return ( d.depth == 0 ? 0 : 0.8)})
				.attr("transform", "translate(0,-5)");
					
				// Transition exiting nodes to the parent's new position.
				var nodeExit = node.exit()
					.transition()
					.duration(duration)
					.attr("transform", function(d) { 
						return "translate(" + source.y + "," + source.x + ")"; 
					})
					.remove();

				nodeExit.select("circle")
						.attr("r", 1e-6);

				node.exit().select("text")
					.transition()
					.duration(duration)
					.style("fill-opacity", 1e-6);					
				node.exit().select("rect")
					.transition()
					.duration(duration)
					.style("fill-opacity", 1e-6)
					.attr("transform", "translate(0,-5)");

				// Update the links…
				var link = svg.selectAll("path.link")
						.data(links, function(d) { return d.target.id; });

				link.enter().insert("path", "g")
					.attr("class", "link")
					.attr("fill", "none" )
					.style("stroke", function(d) { return colorMap[d.target.category]; })
					.attr("d", function(d) {
						var o = {x: source.x0, y: source.y0};
						return diagonal({source: o, target: o});
					})
					
				// Transition links to their new position.
				link
					.transition()
					.duration(duration)
					.attr("d", diagonal);

				// Transition exiting nodes to the parent's new position.
				link.exit()
					.transition()
					.duration(duration)
					.attr("d", function(d) {
						var o = {x: source.x, y: source.y};
						return diagonal({source: o, target: o});
					})
					.remove();

				var activeNodes = [];
				// Stash the old positions for transition.

				nodes.forEach(function(d) {
						activeNodes.push(d);
				    d.x0 = d.x;
				    d.y0 = d.y;
				});
				recenterGraph(nodes, duration);
			}
			
			function getBB(selection) {
				selection.each(function(d){
					d.bbox = this.getBBox();
				})
			};
			
			function recenterGraph(nodes, duration) {
				var minY = 0;
				var maxY = 0;
				nodes.forEach(function(d) { 
						minY = Math.min(minY,d.x);
						maxY = Math.max(maxY,d.x);
				});
				
				d3.select("g")
					.transition()
					.duration(duration)
					.attr("transform", "translate("+margin.left+","+(-minY + margin.top)+")");
					
				height = maxY-minY;
				
				d3.select("svg")
					.transition()
					.duration(duration)
					.attr("height", margin.top + height + margin.bottom);
			}

			function click(d) {
				if((d.depth != 0) && (d.children || d._children)) {
					if (d.children) {
						d._children = d.children;
						d.children = null;
					} else {
						d.children = d._children;
						d._children = null;
					}
					update(d);
				}
			}

			function mouseover(d) {
				if((d.depth != 0) && (d.children || d._children)) {
					d3.select(this).append("text")
						.attr("class", "hover")
						.attr('transform', function(d){ 
							return 'translate(5, -10)';
						})
						.text(d.category);
					d3.select(this).style("cursor", "pointer"); 
				}
			}

			function mouseout(d) {
				if((d.depth != 0) && (d.children || d._children)) {
					d3.select(this).select("text.hover").remove();
					d3.select(this).style("cursor", "default"); 
				}
			}
			
			var inherited = false;
			function toggleInherited(d) {
				if(inherited) {
					d3.select("#inheritedButtonText").text("Show inherited groups");
					d3.select("#inheritedButton").style("fill", "#4ABDAC");
					hideIndirectGroups();
					inherited = false;
				} else {
					d3.select("#inheritedButtonText")
					.text("Show direct groups");
					d3.select("#inheritedButton")
					.style("fill", "#F7B733");
					showIndirectGroups()
					inherited = true;
				}
			}

			function inheritedMO(d) {
				d3.select("#inheritedButton").style("fill-opacity", 0.5);	
				d3.select(this).style("cursor", "pointer"); 
			}

			// Toggle children on click.
			function inheritedO(d) {
				d3.select("#inheritedButton").style("fill-opacity", 1);
				d3.select(this).style("cursor", "default"); 
			}
			
			function hideIndirectGroups() {
				root.children.forEach(function(d) {
					if(d.children) {
						d._children = d.children;
						d.children = null;
					}
				});
				update(root);
			}
			
			function expand(d){   
				var children = (d.children)?d.children:d._children;
				if (d._children) {        
					d.children = d._children;
					d._children = null;       
				}
				if(children)
					children.forEach(expand);
			}
			
			function showIndirectGroups() {
				root.children.forEach(function(d) {
					expand(d);
				});
				update(root);
			}
			
			update(root);
			hideIndirectGroups();
		});
		
		$scope.account = $routeParams.VUnetID;

		let accountMembershipsController = this;
	});
})();

function unflatten(arr, root) {
	var tree = {},
			mappedArr = {},
			arrElem,
			mappedElem;
			
	tree.name = root;
	tree.children = [];

	// First map the nodes of the array to an object -> create a hash table.
	for(var i = 0, len = arr.length; i < len; i++) {
		arrElem = arr[i];
		mappedArr[arrElem.groupId] = arrElem;
		mappedArr[arrElem.groupId]['children'] = [];
	}

	for (var groupId in mappedArr) {
		if (mappedArr.hasOwnProperty(groupId)) {
			mappedElem = mappedArr[groupId];
			// If the element is not at the root level, add it to its parent array of children.
			if (mappedElem.parentId) {
				mappedArr[mappedElem['parentId']]['children'].push(mappedElem);
			}
			// If the element is at the root level, add it to first level elements array.
			else {
				tree['children'].push(mappedElem);
			}
		}
	}

	return tree;
}

function analyzeMemberships(data) {	
	var parents = [];
	var leaves = [];	
	var leafCount = 0;
	
	for(var i=0; i<data.length; i++){
		leaves.push(data[i].groupId);
		if(data[i].parentId != null){
			parents.push(data[i].parentId);
		}


	}
	for(var i=0; i<leaves.length; i++) {
		if(!parents.includes(leaves[i])){
			leafCount++;
		}
	}
	
	return leafCount;
}

function calculateMaxNameLengths(data, user) {
	var maxDepth = 0;
	var cw = 5.2; // character-width
	var leftPadding = 150;
	var padding = 40;
	
	// Get highest depth of groupmembership.
	for(var i=0; i<data.length; i++){
		maxDepth = Math.max(data[i].depth+2, maxDepth);
	}
	
	var depth = [];
	depth[0] = user.length;
	depth[1] = leftPadding;
	// Fill array with zero's
	for(var i=2; i<maxDepth+1; i++){
		// Fill first index with length of the name of the user.
		depth[i] = 0;
	}
	
	for(var i=0; i<data.length; i++){
		// Store max name length at index of depth array.
		depth[data[i].depth+2] = Math.max(depth[data[i].depth+2],Math.floor(data[i].name.length*cw));
	}
	
	for(var i=1; i<maxDepth+1; i++){
		depth[i] += depth[i-1] + 60;
	}
	
	return depth;
}

var colorMap = {};
		colorMap.Employee="#1F78B4";
		colorMap.Student="#FF7D0A";
		colorMap.Fileserver="#2AA12D";
		colorMap.Mail="#D52728";
		colorMap.Printing="#9467BC";
		colorMap.Software="#8D564A";
		colorMap.Resources="#E577C0";
		colorMap.Cloud="#7F7F7F";
		colorMap.Citrix="#BCBD24";
		colorMap.Website="#18BDD0";
		colorMap.Service="#AEC6E8";
		colorMap.Server="#FFBC78";
		colorMap.Computer="#97DF89";
		colorMap.System="#FF9693";
		colorMap.Unspecified="#C2B0D7";
