'use strict';

module.exports = async function(Adgroup) {
	Adgroup.groupsByUIG = function(id,cb) {
		var ds = Adgroup.dataSource;
		
		Adgroup.category = "";
		
		var sql = 'select adgroup.objectGUID, adgroup.cn, adgroup.description, adgroup.distinguishedName, '+
							'adgroup.uSNChanged, adgroup.functionalDescription, groupcategory.name "category", aduig.row_start, aduig.row_end '+
							'from aduig FOR SYSTEM_TIME ALL '+
							'left join adgroup on adgroup.objectGUID = aduig.memberOfId '+
							'left join groupcategory on adgroup.categoryId = groupcategory.id '+
							'where memberId="'+id+'" '+
							'order by groupcategory.sort, adgroup.cn asc;'
		
		ds.connector.execute(sql, null, function (err, relations) {
				if (err) console.error(err);
				cb(err, relations);
		});
	}
	
	Adgroup.remoteMethod('groupsByUIG', {
		http: { source: 'path', verb: 'get'},
		description: 'Get all group memberships for user including versioning.',
		accepts: {arg: 'id', type: 'string' },
		returns: {arg: 'results', type: ['object'], root: true}
	});
};