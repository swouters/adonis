'use strict';
//var srv = require('../../server/server');

module.exports = function(Adsynclog) {
	Adsynclog.getLast = function(cb) {
		//var ds = Adsynclog.dataSource;
		
		Adsynclog.find({limit: 1, order: 'id DESC'},function (err, asl) {
				if (err) console.error(err);
				cb(err, asl[0]);
		});
	}
	
	Adsynclog.remoteMethod('getLast', {
		http: { source: 'path', verb: 'get' },
		description: "Get the most recent adsynclog entry",
		returns: {type: 'adsynclog', root: true}
	});	
};
