'use strict';

module.exports = async function(Adaccount) {
	Adaccount.groupsByUIG = function(account,cb) {
		var ds = Adaccount.dataSource;
		
		var sql = "SELECT depth, adgroup.cn as 'name', memberOfId as 'groupId', memberId as 'parentId', groupCategory.name as 'category' from (  \
WITH RECURSIVE groups(depth, memberId, memberOfId) AS  \
( \
	SELECT 1 AS depth, memberId, memberOfId  \
	FROM adgig \
	WHERE memberId IN ( \
		SELECT o.memberOfId \
		FROM aduig AS o \
		WHERE o.memberId = ( \
			SELECT objectGUID  \
			FROM adaccount  \
			WHERE cn = '"+account+"' \
		) \
	) \
 \
UNION \
 \
	SELECT depth+1, a.memberId, a.memberOfId \
	FROM adgig AS a, groups AS g \
	WHERE a.memberId = g.memberOfId \
) \
 \
SELECT depth, memberId, memberOfId \
FROM groups \
 \
UNION \
 \
SELECT 0 as 'depth', null as memberId, memberOfId \
FROM aduig \
WHERE memberId = ( \
	SELECT objectGUID  \
	FROM adaccount  \
	WHERE cn = '"+account+"' \
) \
 \
) AS structure, adgroup, groupcategory  \
WHERE memberOfId = adgroup.objectGUID  \
AND adgroup.categoryId = groupcategory.id \
ORDER BY groupcategory.id, adgroup.cn";
		
		ds.connector.execute(sql, null, function (err, relations) {
				if (err) console.error(err);
				cb(err, relations);
		});
	}
	
	Adaccount.remoteMethod('groupsByUIG', {
		http: { source: 'path', verb: 'get'},
		description: 'Get all group memberships for user including versioning.',
		accepts: {arg: 'account', type: 'string' },
		returns: {arg: 'results', type: ['object'], root: true}
	});
};