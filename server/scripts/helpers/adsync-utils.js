'use strict';

function formatGUID(objectGUID) {
  var data = Buffer.from(objectGUID, 'binary');

  // GUID_FORMAT_D
  var template = '{3}{2}{1}{0}-{5}{4}-{7}{6}-{8}{9}-{10}{11}{12}{13}{14}{15}';

  // check each byte
  for (var i = 0; i < data.length; i++) {
    // get the current character from that byte
    var dataStr = data[i].toString(16);
    dataStr = data[i] >= 16 ? dataStr : '0' + dataStr;

    // insert that character into the template
    template = template.replace(
			new RegExp(
				'\\{' + i + '\\}', 'g'
			), dataStr
		).toUpperCase();
  }

  return template;
}
exports.formatGUID = formatGUID;

function groupsToGUIDs(list, callback) {
  if (list) {
    var result = [];
    list.forEach(function(object) {
      if (object.objectGUID) {
        result.push(object.objectGUID);
      }
    });
    return callback(null, result);
  } else {
    return callback(null, []);
  }
}
exports.groupsToGUIDs = groupsToGUIDs;

function trimDN(inputStr) {
  if (inputStr.length != 0) {
    var result = inputStr.substr(
      inputStr.indexOf('CN=') + 3,
      inputStr.indexOf(',') - 3
    );
    return result;
  } else {
    return inputStr;
  }
}
exports.trimDN = trimDN;

function uacToStatus(uac) {
  if (uac & 2) {
    return 'disabled';
  } else {
    return 'enabled';
  }
}
exports.uacToStatus = uacToStatus;

function filterObjectLists(srcList, destList, ht, type, deb, callback) {
  deb('Start filtering %s into add, update and remove lists.', type);
  var addList = [];
  var updateList = [];
  var removeList = [];

  srcList = srcList.sort(compareObjects);
  destList = destList.sort(compareObjects);

  var slIndex = 0;
  var dlIndex = 0;

  while (slIndex < srcList.length && dlIndex < destList.length) {
    if (srcList[slIndex].objectGUID < destList[dlIndex].objectGUID) {
      // add
      addList.push(srcList[slIndex]);
      slIndex++;
    } else if (srcList[slIndex].objectGUID == destList[dlIndex].objectGUID) {
      // update
      // Only update if uSNChanged is different.
      if (srcList[slIndex].uSNChanged != destList[dlIndex].uSNChanged) {
        updateList.push(srcList[slIndex]);
      }
      slIndex++;
      dlIndex++;
    } else {
      // remove
      removeList.push(destList[dlIndex]);
			ht.put(destList[dlIndex].objectGUID, destList[dlIndex].cn);
      dlIndex++;
    }
  }

  // if both indices are at the end of their respective list; don't do the following.
  if (slIndex != srcList.length || dlIndex != destList.length) {
    // if the source-index is at the end of its list, add all of the remaining destination groups.
    if (slIndex == srcList.length) {
      // remove dest remainder
      while (dlIndex < destList.length) {
        removeList.push(destList[dlIndex]);
				ht.put(destList[dlIndex].objectGUID, destList[dlIndex].cn);
        dlIndex++;
      }
      // if the destination-index is at the end of its list, add all of the remaining source groups.
    } else if (dlIndex == destList.length) {
      // add src remainder
      while (slIndex < srcList.length) {
        addList.push(srcList[slIndex]);
        slIndex++;
      }
    }
  }

  deb('Finished filtering %s. (Add: %s; Update: %s; Remove: %s)',
		type, addList.length, updateList.length, removeList.length);

  return callback(null, {
    add: addList,
    update: updateList,
    remove: removeList,
		htlu: ht}
	);
}
exports.filterObjectLists = filterObjectLists;

function filterRelationLists(srcList, destList, type, deb, callback) {
  deb('Start filtering %s relations into add and remove lists.', type);
  var addList = [];
  var removeList = [];

  srcList = srcList.sort(compareRelations);
  destList = destList.sort(compareRelations);

  var slIndex = 0;
  var dlIndex = 0;

  while (slIndex < srcList.length && dlIndex < destList.length) {
    if (srcList[slIndex].memberId < destList[dlIndex].memberId) {
      // add
      addList.push(srcList[slIndex]);
      slIndex++;
    } else if (srcList[slIndex].memberId == destList[dlIndex].memberId) {
      if (srcList[slIndex].memberOfId == destList[dlIndex].memberOfId) {
        // do nothing
        slIndex++;
        dlIndex++;
      } else if (srcList[slIndex].memberOfId < destList[dlIndex].memberOfId) {
        addList.push(srcList[slIndex]);
        slIndex++;
      } else {
        removeList.push(destList[dlIndex]);
        dlIndex++;
      }
    } else {
      removeList.push(destList[dlIndex]);
      dlIndex++;
    }
  }

  // if both indices are at the end of their respective list; don't do the following.
  if (slIndex != srcList.length || dlIndex != destList.length) {
    // if the source-index is at the end of its list, add all of the remaining destination groups.
    if (slIndex == srcList.length) {
      // remove dest remainder
      while (dlIndex < destList.length) {
        removeList.push(destList[dlIndex]);
        dlIndex++;
      }
      // if the destination-index is at the end of its list, add all of the remaining source groups.
    } else if (dlIndex == destList.length) {
      // add src remainder
      while (slIndex < srcList.length) {
        addList.push(srcList[slIndex]);
        slIndex++;
      }
    }
  }

  deb('Finished filtering %s relations. (Add: %s; Remove: %s)',
		type, addList.length, removeList.length);
  return callback(null, {
    add: addList,
    remove: removeList}
	);
}
exports.filterRelationLists = filterRelationLists;

function compareObjects(a, b) {
  if (a.objectGUID < b.objectGUID)
    return -1;
  if (a.objectGUID > b.objectGUID)
    return 1;
  return 0;
}

function compareRelations(a, b) {
  if (a.memberId == b.memberId) {
    if (a.memberOfId == b.memberOfId) {
      return 0; // memberId and memberOfId are the same
    }
    if (a.memberOfId < b.memberOfId) {
      return -1; // memberId is the same and memberOfId is smaller
    }
    return 1; // memberId is the same and memberOfId is smaller
  }
  if (a.memberId < b.memberId) {
    return -1; // memberId is smaller
  }
  return 1; // memberId is larger
}
exports.compareRelations = compareRelations;
