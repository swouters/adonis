'use strict';

var path = require('path');
global.appRoot = path.resolve(__dirname);
var dotenv = require('dotenv').config({path: appRoot+"\\..\\..\\.env"}); // config file using dotenv

var app = require('../server');
var async = require('async');
var ldap = require('ldapjs');
var assert = require('assert');
var hashtable = require('simple-hashtable');

var nodemailer = require('nodemailer');

var eachLimit = 5000;

var helpers = require('./helpers/adsync-utils');

const debug = require('debug')('ADsync');

const debugPhase = debug.extend('PHASE');

const debugAD = debug.extend('Active Directory');
const debugADGroups = debugAD.extend('groups');
const debugADAccounts = debugAD.extend('accounts');

const debugDB = debug.extend('Database');
const debugDBGroups = debugDB.extend('groups');
const debugDBGroupsadd = debugDBGroups.extend('add');
const debugDBGroupsupdate = debugDBGroups.extend('update');
const debugDBGroupsremove = debugDBGroups.extend('remove');
const debugDBAccounts = debugDB.extend('accounts');
const debugDBAccountsadd = debugDBAccounts.extend('add');
const debugDBAccountsupdate = debugDBAccounts.extend('update');
const debugDBAccountsremove = debugDBAccounts.extend('remove');

const debugDBGIGs = debugDB.extend('GIGs');
const debugDBGIGsAdd = debugDBGIGs.extend('add');
const debugDBGIGsRemove = debugDBGIGs.extend('remove');
const debugDBUIGs = debugDB.extend('UIGs');
const debugDBUIGsAdd = debugDBUIGs.extend('add');
const debugDBUIGsRemove = debugDBUIGs.extend('remove');

const debugScript = debug.extend('Script');
const debugScriptGroups = debugScript.extend('Groups');
const debugScriptAccounts = debugScript.extend('Accounts');
const debugScriptGIGs = debug.extend('GIG');
const debugScriptGIGsAdd = debugScriptGIGs.extend('Add');
const debugScriptGIGsUpdate = debugScriptGIGs.extend('Update');
const debugScriptGIGsRemove = debugScriptGIGs.extend('Remove');
const debugScriptUIGs = debug.extend('UIG');

/* Active Directory Production connection setting */
/* Retrieved using npm dotenv */
var server = process.env.SERVER;
var userPrincipalName = process.env.USERPRINCIPLENAME;
var password = process.env.PASSWORD;
var adSuffix = process.env.ADSUFFIX;

var client;

var groupFilter = '(&(objectClass=group)(!(objectClass=computer))(!(objectClass=user))(!(objectClass=person)))';
var accountFilter = '(&(!(objectClass=group))(!(objectClass=computer))(objectClass=user)(objectClass=person))';

function init() {
  debug('Starting Active Directory Sync');

  bindLDAP();
  initiateBeforeSaveHook();

  // ======================================================================= //
  //                                                                         //
  //                              PHASE A                                    //
  //                                                                         //
  //   1. Retrieve groups from the Database.                                 //
  //   2. Retrieve groups from Active Directory.                             //
  //   3. Retrieve accounts from the Database.                               //
  //   4. Retrieve accounts from Active Directory.                           //
  //   5. Retrieve User-in-Group relations from the Database.                //
  //   6. Retrieve Group-in-Group relations from the Database.               //
  //                                                                         //
  // ======================================================================= //

  printDebugPhase(debugPhase, 'A', true);
  async.parallel([
    retrieveDBGroups,
    retrieveADGroups,
    retrieveDBAccounts,
    retrieveADAccounts,
    retrieveDBRelations.bind(null, app.models.aduig, 'User-in-Group',	debugDBUIGs),
    retrieveDBRelations.bind(null, app.models.adgig, 'Group-in-Group', debugDBGIGs)],
    function(err, results) {
      if (err) {
        console.log(err);
      }
			unbindLDAP();
      printDebugPhase(debugPhase, 'A', false);

      // ======================================================================= //
      //                                                                         //
      //                              PHASE B                                    //
      //                                                                         //
      //   1. Store retrieved AD en DB objects.                                  //
      //                                                                         //
      // ======================================================================= //
      printDebugPhase(debugPhase, 'B', true);
      var DBGroups = results[0];
      var DBAccounts = results[2];
      var ADGroups = results[1]['queue'];
      var ADAccounts = results[3]['queue'];
      var groupHashTable = results[1]['htdo'];
			var groupLookupTable = results[1]['htlu'];
			var accountLookupTable = results[3]['htlu'];
      var DBUIGs = results[4];
      var DBGIGs = results[5];
      printDebugPhase(debugPhase, 'B', false);

      // ======================================================================= //
      //                                                                         //
      //                              PHASE C                                    //
      //                                                                         //
      //   1. Filter accounts into Add/Update/Remove lists.                      //
      //   2. Filter accounts into Add/Update/Remove lists.                      //
      //   3. Extract User-in-Group relations from AD users.                     //
      //   4. Extract Group-in-Group relations from new groups.                  //
      //                                                                         //
      // ======================================================================= //
      printDebugPhase(debugPhase, 'C', true);
      async.parallel([
				helpers.filterObjectLists.bind(null, ADGroups, DBGroups, groupLookupTable, 'groups', debugScriptGroups),
        helpers.filterObjectLists.bind(null, ADAccounts,	DBAccounts, accountLookupTable,	'accounts',	debugScriptAccounts),
        extractRelations.bind(null, ADAccounts, groupHashTable, 'User-in-Group',	debugScriptUIGs),
        extractRelations.bind(null,	ADGroups,	groupHashTable,	'Group-in-Group',	debugScriptGIGs)],
        function(err, results) {
          if (err) {
            console.log(err);
          }
          printDebugPhase(debugPhase, 'C', false);

          var addGroupList = results[0]['add'];
          var updateGroupList = results[0]['update'];
          var removeGroupList = results[0]['remove'];
					groupLookupTable = results[0]['htlu'];			

          var addAccountList = results[1]['add'];
          var updateAccountList = results[1]['update'];
          var removeAccountList = results[1]['remove'];
					accountLookupTable = results[1]['htlu'];

          var ADUIGs = results[2];
          var ADGIGs = results[3];

          // ======================================================================= //
          //                                                                         //
          //                              PHASE D                                    //
          //                                                                         //
          //   1. Add new ADgroups to the Database.                                  //
          //   2. Update existing ADgroups in the Database.                          //
          //   3. Remove missing ADgroups from the Database.                         //
          //   4. Add new ADaccounts to the Database.                                //
          //   5. Update existing ADaccounts in the Database.                        //
          //   6. Remove missing ADaccounts from the Database.                       //
          //   7. Filter User-in-Group relations into Add/Remove lists.              //
          //   8. Filter Group-in-Group relations into Add/Remove lists.             //
          //                                                                         //
          // ======================================================================= //
          printDebugPhase(debugPhase, 'D', true);
          async.parallel([
            upsertGroup.bind(null, addGroupList, 'group', 'add', debugDBGroupsadd),
            upsertGroup.bind(null, updateGroupList, 'group', 'update', debugDBGroupsupdate),
            removeGroup.bind(null, removeGroupList, 'group', 'remove', debugDBGroupsremove),

            upsertAccount.bind(null, addAccountList, 'account', 'add', debugDBAccountsadd),
            upsertAccount.bind(null, updateAccountList, 'account', 'update', debugDBAccountsupdate),
            removeAccount.bind(null, removeAccountList, 'account', 'remove', debugDBAccountsremove),

            helpers.filterRelationLists.bind(null, ADUIGs, DBUIGs, 'User-in-Group', debugScriptUIGs),
            helpers.filterRelationLists.bind(null, ADGIGs, DBGIGs, 'Group-in-Group', debugScriptGIGs)],
            function(err, result) {
              if (err) {
                console.log(err);
              }
              printDebugPhase(debugPhase, 'D', false);
              var userInGroupRelationsAdd = result[6]['add'];
              var userInGroupRelationsRemove = result[6]['remove'];
              var groupInGroupRelationsAdd = result[7]['add'];
              var groupInGroupRelationsRemove = result[7]['remove'];

              // ======================================================================= //
              //                                                                         //
              //                              PHASE E                                    //
              //                                                                         //
              //   1. Add new User-in-Group relations to the database.                   //
              //   2. Remove missing User-in-Group relations from the database.          //
              //   3. Add new Group-in-Group relations to the database.                  //
              //   4. Remove missing Group-in-Group relations from the database.         //
              //                                                                         //
              // ======================================================================= //
              printDebugPhase(debugPhase, 'E', true);
              async.parallel([
                insertObjectRelation.bind(null, userInGroupRelationsAdd, app.models.aduig, 'User-in-Group', debugDBUIGsAdd),
                removeObjectRelation.bind(null, userInGroupRelationsRemove, app.models.aduig, 'User-in-Group', debugDBUIGsRemove),
                insertObjectRelation.bind(null, groupInGroupRelationsAdd, app.models.adgig, 'Group-in-Group', debugDBGIGsAdd),
                removeObjectRelation.bind(null, groupInGroupRelationsRemove, app.models.adgig, 'Group-in-Group', debugDBGIGsRemove)],
                function(err, results) {
                  if (err) {
                    console.log(err);
                  }
                  printDebugPhase(debugPhase, 'E', false);
									
              // ======================================================================= //
              //                                                                         //
              //                              PHASE F                                    //
              //                                                                         //
              //   1. Update adgroup hasParent attribute for all groups.                 //
              //   2. Insert adsynclog into database.                                    //
              //   3. Send log e-mail.                                                   //										
              //                                                                         //
              // ======================================================================= //									
									printDebugPhase(debugPhase, 'F', true);									
									debugDBGroups('Starting group hasParent calculation.');

									var sql = 'update adgroup set hasParent = CASE WHEN objectGUID in (select distinct memberId from adgig) THEN 1 ELSE 0 END';									
									app.models.adgroup.dataSource.connector.execute(sql, null, function(err) {
										if (err) {
											console.log(err);
										}
										debugDBGroups('Finished group hasParent calculation.');																														
										debugScript('Inserting adsynclog.');
										
										insertLog(
											addAccountList, updateAccountList, removeAccountList,
											addGroupList, updateGroupList, removeGroupList,
											userInGroupRelationsAdd, userInGroupRelationsRemove,
											groupInGroupRelationsAdd, groupInGroupRelationsRemove,
											accountLookupTable, groupLookupTable,
											function(err, results) {
												if (err) {
													console.log(err);
												}
												debugScript('Sending adsynclog email.');
												sendReport(
													addAccountList.length,
													updateAccountList.length,
													removeAccountList.length,
													addGroupList.length,
													updateGroupList.length,
													removeGroupList.length,
													userInGroupRelationsAdd.length,
													userInGroupRelationsRemove.length,
													groupInGroupRelationsAdd.length,
													groupInGroupRelationsRemove.length);
													
												printDebugPhase(debugPhase, 'F', false);		
												app.dataSources.adonisdb.disconnect();
												debug('Finished Active Directory Sync');
											}
										);											
									});	
                });
            });
        });
    });
}

function initiateBeforeSaveHook() {
  //ADAccounts
  app.models.adaccount.observe('before save', async function(ctx) {
    if (ctx.currentInstance) {
      if (ctx.data.objectGUID == ctx.currentInstance.objectGUID) {
        delete ctx.data.objectGUID;
      }
      if (ctx.data.cn == ctx.currentInstance.cn) {
        delete ctx.data.cn;
      }
      if (ctx.data.distinguishedName == ctx.currentInstance.distinguishedName) {
        delete ctx.data.distinguishedName;
      }
      if (ctx.data.userAccountControl == ctx.currentInstance.userAccountControl) {
        delete ctx.data.userAccountControl;
      }
    }
    return;
  });

  //ADGroup
  app.models.adgroup.observe('before save', async function(ctx) {
    if (ctx.currentInstance) {
      if (ctx.data.objectGUID == ctx.currentInstance.objectGUID) {
        delete ctx.data.objectGUID;
      }
      if (ctx.data.cn == ctx.currentInstance.cn) {
        delete ctx.data.cn;
      }
      if (ctx.data.description == ctx.currentInstance.description) {
        delete ctx.data.description;
      }
      if (ctx.data.distinguishedName == ctx.currentInstance.distinguishedName) {
        delete ctx.data.distinguishedName;
      }
    }
    return;
  });
}

function extractRelations(list, ht, type, deb, cb) {
  deb('Start extraction of %s relations from ADaccounts.', type);
  var resultList = [];
  async.map(
    list,
    function process(obj, callback) {
      if (obj.memberOf) {
        var container = [];
        if (Array.isArray(obj.memberOf)) {
          for (var i = 0; i < obj.memberOf.length; i++) {
            var htLookup = ht.get(obj.memberOf[i]);
            if (htLookup != -1) { // Ignore memberOf relations that can't be found in hashtable.
              container.push({
                memberId: obj.objectGUID,
                memberOfId: htLookup
              });
            }
          }
          callback(null, container);
        } else {
          var htLookup = ht.get(obj.memberOf);
          if (htLookup != -1) { // Ignore memberOf relations that can't be found in hashtable.
            container.push({
              memberId: obj.objectGUID,
              memberOfId: htLookup
            });
          }
          callback(null, container);
        }
      } else {
        callback(null);
      }
    },
    function(err, results) {
      if (err) {
        console.log(err);
      }
      for (var i = 0; i < results.length; i++) {
        if (results[i]) {
          for (var j = 0; j < results[i].length; j++) {
            resultList.push(results[i][j]);
          }
        }
      }

      deb('Finished extracting %s relations from ADaccounts. (%s)', type, resultList.length);
      return cb(null, resultList);
    }
  );
}

function upsertGroup(list, type, operation, deb, cb) {
  if (operation == 'add') {
    deb('Start adding %ss to the database. (%s)', type, list.length);
  } else {
    deb('Start updating %ss to the database. (%s)', type, list.length);
  }

  var model = app.models.adgroup;
  async.eachLimit(
    list,
		eachLimit,
    function process(group, callback) {
			var catId = determineGroupCategory(group.cn, group.distinguishedName);
      model.upsertWithWhere({
        objectGUID: group.objectGUID
      }, {
        objectGUID: group.objectGUID,
        cn: group.cn,
        description: group.description,
        distinguishedName: group.distinguishedName,
        uSNChanged: group.uSNChanged,
				categoryId: catId
      }, function(err, log) {
        if (err) {
          return callback(err, log);
        };
        return callback(null);
      });
    },
    function(err) {
      if (err) {
        console.log(err);
      }

      if (operation == 'add') {
        deb('Finished adding %ss. Added %s %ss to the database.', type, list.length, type);
      } else {
        deb('Finished updating %ss. Updated %s %ss to the database.', type, list.length, type);
      }

      return cb(null, list);
    }
  );
}

function removeGroup(list, type, operation, deb, cb) {
  deb('Start deleting %ss from the database. (%s)', type, list.length);

  var model = app.models.adgroup;
  async.eachLimit(
    list,
		eachLimit,
    function process(group, callback) {
      model.destroyById(group.objectGUID, function(err, log) {
        if (err) {
          return callback(err, log);
        };
        return callback(null);
      });
    },
    function(err) {
      if (err) {
        console.log(err);
      }

      deb('Finished deleting %ss. Deleted %s %ss from the database.', type, list.length, type);
      return cb(null, list);
    }
  );
}

function upsertAccount(list, type, operation, deb, cb) {
  if (operation == 'add') {
    deb('Start adding %ss to the database. (%s)', type, list.length);
  } else {
    deb('Start updating %ss to the database. (%s)', type, list.length);
  }

  var model = app.models.adaccount;
  async.eachLimit(
    list,
		eachLimit,
    function process(account, callback) {
      model.upsertWithWhere({
        objectGUID: account.objectGUID
      }, {
        objectGUID: account.objectGUID,
        cn: account.cn,
        distinguishedName: account.distinguishedName,
        userAccountControl: account.userAccountControl,
        uSNChanged: account.uSNChanged
      }, function(err, log) {
        if (err) {
          return callback(err, log);
        };
        return callback(null);
      });
    },
    function(err) {
      if (err) {
        console.log(err);
      }

      if (operation == 'add') {
        deb('Finished adding %ss. Added %s %ss to the database.', type, list.length, type);
      } else {
        deb('Finished updating %ss. Updated %s %ss to the database.', type, list.length, type);
      }

      return cb(null, list);
    }
  );
}

function removeAccount(list, type, operation, deb, cb) {
  deb('Start deleting %ss from the database. (%s)', type, list.length);

  var model = app.models.adaccount;
  async.eachLimit(
    list,
		eachLimit,
    function process(account, callback) {
      model.destroyById(account.objectGUID, function(err, log) {
        if (err) {
          return callback(err, log);
        };
        return callback(null);
      });
    },
    function(err) {
      if (err) {
        console.log(err);
      }

      deb('Finished deleting %ss. Deleted %s %ss from the database.', type, list.length, type);
      return cb(null, list);
    }
  );
}

function insertObjectRelation(list, model, type, deb, cb) {
  deb('Starting insertion of %s relations to the database. (%s)', type, list.length);
  async.eachLimit(
    list,
		eachLimit,
    function process(group, callback) {
      model.findOrCreate({
        where: {
          memberId: group.memberId,
          memberOfId: group.memberOfId
        }
      }, {
        memberId: group.memberId,
        memberOfId: group.memberOfId
      }, function(err, log) {
        if (err) {
          return callback(null);
        };
        return callback(null);
      });
    },
    function(err) {
      if (err) {
        console.log(err);
      }

      deb('Finished inserting %s relations. Inserted %s records into the database.', type, list.length);
      return cb(null, list);
    }
  );
}

function removeObjectRelation(list, model, type, deb, cb) {
  deb('Starting removal of %s relations from the database. (%s)', type, list.length);

  async.eachLimit(
    list,
		eachLimit,
    function process(account, callback) {
      model.destroyAll({
        memberId: account.memberId,
        memberOfId: account.memberOfId
      }, function(err, log) {
        if (err) {
          return callback(null);
        };
        return callback(null);
      });
    },
    function(err) {
      if (err) {
        console.log(err);
      }

      deb('Finished removing %s relations. Removed %s records from the database.', type, list.length);
      return cb(null, list);
    }
  );
}

function retrieveDBGroups(callback) {
  debugDBGroups('Start retrieving groups.');
  app.models.adgroup.find({}, function(err, res) {
    if (err) {
      return callback(err, res);
    }
    debugDBGroups('Finished retrieving groups. Retrieved %s groups from database.', res.length);
    return callback(null, res);
  });
}

function retrieveDBAccounts(callback) {
  debugDBAccounts('Start retrieving accounts.');
  app.models.adaccount.find({}, function(err, res) {
    if (err) {
      return callback(err, res);
    }
    debugDBAccounts('Finished retrieving accounts. Retrieved %s accounts from database.', res.length);
    return callback(null, res);
  });
}

function retrieveDBRelations(model, type, deb, cb) {
  deb('Start retrieving %s relations from database.', type);
  model.find({}, function(err, res) {
    if (err) {
      return cb(err, res);
    }
    deb('Finished retrieving %s relations. Retrieved %s relations from database.', type, res.length);
    return cb(null, res);
  });
}

function retrieveADGroups(callback) {
  debugADGroups('Start retrieving groups.');

  const searchOptions = {
    scope: 'sub',
    filter: groupFilter,
    attributes: [
      'objectGUID',
      'uSNChanged',
      'cn',
      'description',
      'distinguishedName',
      'memberOf'
    ],
    paged: {
      pageSize: 1000,
      pagePause: true
    }
  };


  var htdo = new hashtable();
	var htlu = new hashtable();
  var queue = [];

  client.search(adSuffix, searchOptions, (err, res) => {
    assert.ifError(err);
    res.on('searchEntry', function(entry) {
      var obj = entry.object;
      obj.objectGUID = helpers.formatGUID(entry.raw.objectGUID);
      htdo.put(obj.distinguishedName, obj.objectGUID);
			htlu.put(obj.objectGUID, obj.cn);
      queue.push(obj);
    });
    res.on('page', function(result, cb) {
      // Allow the queue to flush before fetching next page
      if (cb) {
        cb.call();
      } else {}
    });
    res.on('error', function(resErr) {
      assert.ifError(resErr);
      callback(null)
    });
    res.on('end', result => {
      debugADGroups('Finished retrieving groups. Retrieved %s groups from Active Directory.', queue.length);
      callback(null, {
        queue,
        htdo,
				htlu
      });
    });
  })
}

function retrieveADAccounts(callback) {
  debugADAccounts('Start retrieving accounts.');

  const searchOptions = {
    scope: 'sub',
    filter: accountFilter,
    attributes: [
      'objectGUID',
      'distinguishedName',
      'uSNChanged',
      'cn',
      'userAccountControl',
      'memberOf'
    ],
    paged: {
      pageSize: 1000,
      pagePause: true
    }
  };

	var htlu = new hashtable();
  var queue = [];

  client.search(adSuffix, searchOptions, (err, res) => {
    assert.ifError(err);

    res.on('searchEntry', function(entry) {
      // Submit incoming objects to queue
      var obj = entry.object;
      obj.objectGUID = helpers.formatGUID(entry.raw.objectGUID);
			htlu.put(obj.objectGUID, obj.cn);
      queue.push(obj);
    });
    res.on('page', function(result, cb) {
      // Allow the queue to flush before fetching next page
      if (cb) {
        cb.call();
      } else {}
    });
    res.on('error', function(resErr) {
      assert.ifError(resErr);
      callback(null)
    });
    res.on('end', result => {
      debugADAccounts('Finished retrieving accounts. Retrieved %s accounts from Active Directory.', queue.length);
      callback(null, {
        queue,
				htlu
      });
    });
  });
}

function bindLDAP() {
  client = ldap.createClient({
    url: server
  });

  client.bind(userPrincipalName, password, err => {
    assert.ifError(err);
  });
}

function unbindLDAP() {
  client.unbind(function(err) {
    assert.ifError(err);
  });
}

function printDebugPhase(deb, phase, start) {
  if (start) {
    console.log(' ');
    deb('######################## STARTING PHASE ' + phase + ' ########################');
  } else {
    deb('######################## FINISHED PHASE ' + phase + ' ########################');
    console.log(' ');
  }
}

function sendReport(
	addAccount,
	updateAccount,
	removeAccount,
	addGroup,
	updateGroup,
	removeGroup,
	userInGroupRelationsAdd,
	userInGroupRelationsRemove,
	groupInGroupRelationsAdd,
	groupInGroupRelationsRemove
) {
	var body = 'Accounts added: '+addAccount+'\n';
	body += 'Accouns updated: '+updateAccount+'\n';
	body += 'Accounts removed: '+removeAccount+'\n\n';
	body += 'Groups added: '+addGroup+'\n';
	body += 'Groups updated: '+updateGroup+'\n';
	body += 'Groups removed:'+removeGroup+'\n\n';
	body += 'User-in-Group relations added: '+userInGroupRelationsAdd+'\n';
	body += 'User-in-Group relations removed: '+userInGroupRelationsRemove+'\n\n';
	body += 'Group-in-Group relations added: '+groupInGroupRelationsAdd+'\n';
	body += 'Group-in-Group relations removed: '+groupInGroupRelationsRemove;
	
	var transporter = nodemailer.createTransport({
		service: 'gmail',
		auth: {
			user: 'adonis.mailer@gmail.com',
			pass: 'tooprettydaemon'
		}
	});

	var mailOptions = {
		from: 'sjaak.wouters@gmail.com',
		to: 'sjaak.wouters@vu.nl',
		subject: 'ADonis - ADsync report',
		text: body
	};

	transporter.sendMail(mailOptions, function(error, info){	
		if (error) {
			console.log(error);
		} else {
			console.log('Email sent: ' + info.response);
			transport.close();
		}
	});
}

function insertLog(
	addAccount,updateAccount,removeAccount,
	addGroup,updateGroup,removeGroup,
	addUIG,removeUIG,
	addGIG,removeGIG,
	accountLookupTable, groupLookupTable,
	callback
) {
	var	userAddList = logParseObjects(addAccount);
	var userAddCount = userAddList.length;
	userAddList = JSON.stringify(userAddList);
	
	var	userUpdateList = logParseObjects(updateAccount);
	var userUpdateCount = userUpdateList.length;
	userUpdateList = JSON.stringify(userUpdateList);
	
	var userRemoveList = logParseObjects(removeAccount);
	var userRemoveCount = userRemoveList.length;
	userRemoveList = JSON.stringify(userRemoveList);
	
	var	groupAddList = logParseObjects(addGroup);
	var groupAddCount = groupAddList.length;
	groupAddList = JSON.stringify(groupAddList);
	
	var	groupUpdateList = logParseObjects(updateGroup);
	var groupUpdateCount = groupUpdateList.length;
	groupUpdateList = JSON.stringify(groupUpdateList);
	
	var groupRemoveList = logParseObjects(removeGroup);
	var groupRemoveCount = groupRemoveList.length;
	groupRemoveList = JSON.stringify(groupRemoveList);
	
	var	uigAddList = logParseUIGRelations(addUIG, accountLookupTable, groupLookupTable);
	var	uigAddCount = uigAddList.length;
	uigAddList = JSON.stringify(uigAddList);
	
	var	uigRemoveList = logParseUIGRelations(removeUIG, accountLookupTable, groupLookupTable);
	var	uigRemoveCount = uigRemoveList.length;
	uigRemoveList = JSON.stringify(uigRemoveList);
	
	var	gigAddList = logParseGIGRelations(addGIG, groupLookupTable);
	var	gigAddCount = gigAddList.length;
	gigAddList = JSON.stringify(gigAddList);
	
	var	gigRemoveList = logParseGIGRelations(removeGIG, groupLookupTable);
	var	gigRemoveCount = gigRemoveList.length;
	gigRemoveList = JSON.stringify(gigRemoveList);
	
	app.models.adsynclog.create({
		userAddCount: userAddCount,
		userAddList: userAddList,
		userUpdateCount: userUpdateCount,
		userUpdateList: userUpdateList,
		userRemoveCount: userRemoveCount,
		userRemoveList: userRemoveList,
		
		groupAddCount: groupAddCount,
		groupAddList: groupAddList,
		groupUpdateCount: groupUpdateCount,
		groupUpdateList: groupUpdateList,
		groupRemoveCount: groupRemoveCount,
		groupRemoveList: groupRemoveList,	
		
		uigAddCount: uigAddCount,
		uigAddList: uigAddList,
		uigRemoveCount: uigRemoveCount,
		uigRemoveList: uigRemoveList,		
		
		gigAddCount: gigAddCount,
		gigAddList: gigAddList,
		gigRemoveCount: gigRemoveCount,
		gigRemoveList: gigRemoveList		
	}, function(err, log) {
		if (err) {
			return callback(err, log);
		};
		return callback(null);
	});
}

function logParseObjects(list) {
	var result = [];
	for(var i=0;i<list.length;i++){
		result.push(list[i].cn);
	}
	return result;
}

function logParseGIGRelations(list, groupHT) {
	var result = [];
	for(var i=0;i<list.length;i++){
		result.push([
			groupHT.get(list[i].memberId),
			groupHT.get(list[i].memberOfId)
		]);
	}
	return result;
}

function logParseUIGRelations(list, accountHT, groupHT) {
	var result = [];
	for(var i=0;i<list.length;i++){
		result.push([
			accountHT.get(list[i].memberId),
			groupHT.get(list[i].memberOfId)
		]);
	}
	return result;
}

function determineGroupCategory(cn, dn) {
	if(cn && dn) {
		
		/**************/
		/* Fileserver */
		/**************/
		if(dn.includes('OU=Resource Groups,OU=Resources,DC=vu,DC=local') && cn.startsWith('R_GRP')) {
			return 4;
		}
		if(cn.startsWith('U_TSK') || cn.startsWith('U_MDW')) {
			return 4;
		}
		
		/*****************/
		/* Mail & Agenda */
		/*****************/
		if(dn.includes('OU=Groups,OU=MailObjects,DC=vu,DC=local')) {
			return 5;
		}
		if(cn.startsWith('U_MBX') || cn.startsWith('U_MBR')) {
			return 5;
		}
		
		/************/
		/* Printing */
		/************/		
		if(dn.includes('OU=Printer Groups,OU=Resources,DC=vu,DC=local') && cn.startsWith('R_Print')) {
			return 6;
		}	
		if(dn.includes('OU=Printers,OU=User Groups,DC=vu,DC=local') && cn.startsWith('U_Print')) {
			return 6;
		}
		
		/************/
		/* Software */
		/************/			
		if(dn.includes('Application Groups') && cn.startsWith('A_')) {
			return 3;
		}
		if(dn.includes('OU=Applications,OU=User Groups,DC=vu,DC=local') && cn.startsWith('U_APP')) {
			return 3;
		}
		if(dn.includes('OU=Computer Groups,DC=vu,DC=local') && cn.startsWith('C_APP')) {
			return 3;
		}
		
		/*********/
		/* Cloud */
		/*********/			
		if(dn.includes('OU=ITvO,OU=User Groups,DC=vu,DC=local') && cn.startsWith('U_SciStor')) {
			return 7;
		}
		if(dn.includes('OU=ITvO,OU=Resource Groups,OU=Resources,DC=vu,DC=local') && cn.startsWith('R_SciStor')) {
			return 7;
		}
		if(dn.includes('OU=ADFS,OU=Service Groups,OU=Resources,DC=vu,DC=local')) {
			return 7;
		}
		
		/************/
		/* Employee */
		/************/
		if(cn.startsWith('U_Medewerker')) {
			return 8;
		}
		
		/************/
		/* Student */
		/************/
		if(cn.startsWith('U_Student') || cn.startsWith('U_PreStudent')) {
			return 9;
		}
		
		/************/
		/* Computer */
		/************/
		if(dn.includes('OU=Computer Groups,DC=vu,DC=local') && cn.startsWith('C_Computers')) {
			return 10;
		}
		
		/**********/
		/* Server */
		/**********/
		if(dn.includes('OU=Server Groups,OU=Resources,DC=vu,DC=local') && cn.startsWith('C_')) {
			return 11;
		}

		/***********/
		/* Website */
		/***********/
		if(dn.includes('OU=Resource Groups,OU=Resources,DC=vu,DC=local') && cn.startsWith('R_WEB')) {
			return 12;
		}
		if(cn.startsWith('U_WEB')) {
			return 12;
		}
		
		/*************/
		/* Resources */
		/*************/		
		if(
			dn.includes('OU=Resource Groups,OU=Resources,DC=vu,DC=local') && 
			cn.startsWith('R_') &&
			!cn.startsWith('R_GRP') &&
			!cn.startsWith('R_Print') &&
			!cn.startsWith('R_Web') &&
			!cn.startsWith('R_SciStor')
			) {
			return 13;
		}
		
		/**********/
		/* Citrix */
		/**********/
		if(dn.includes('OU=WESOS,OU=Computer Accounts,DC=vu,DC=local') && cn.startsWith('WESOS')) {
			return 14;
		}

		/**********/
		/* System */
		/**********/
		var systemCNs = ['Domain Admins','DnsAdmins','Cert Publishers','Cloneable Domain Controllers',
				'Read-only Domain Controllers','Domain Controllers','Group Policy Creator Owners','Protected Users','Domain Users',
				'Netmon Users','Domain Computers','RAS and IAS Servers','RODC Administrators','Domain Guests','DnsUpdateProxy'];
		if(dn.includes('CN=Builtin,DC=vu,DC=local')) {
			return 15;
		}
		if(systemCNs.includes(cn)) {
			return 15;			
		}

		/***********/
		/* Service */
		/***********/
		if(dn.includes('OU=Service Groups,OU=Resources,DC=vu,DC=local')) {
			return 16;
		}
	}		
	
	/* Unspecified */
	return 1;
}

init();

